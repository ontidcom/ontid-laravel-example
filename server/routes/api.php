<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->namespace('Api\V1')->group(function () {
    Route::middleware('api.auth')->group(function () {
        Route::apiResource('news', 'NewsController')->only(['update', 'store', 'destroy']);
        Route::apiResource('categories', 'CategoryController')->only(['update', 'store', 'destroy']);

        // Media
        Route::apiResource('media', 'MediaController')->only(['store', 'update', 'destroy']);
    });

    Route::apiResource('news', 'NewsController')->only(['index', 'show']);
    Route::apiResource('categories', 'CategoryController')->only(['index', 'show']);

    // Media
    Route::apiResource('media', 'MediaController')->only('index', 'show');
});
