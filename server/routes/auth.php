<?php

Route::prefix('v1')->namespace('Api\V1')->group(function () {
    Route::post('login', 'Auth\AuthenticateController@login')->name('api.login');
    Route::post('logout', 'Auth\AuthenticateController@logout')->name('api.logout');
    Route::post('refresh', 'Auth\AuthenticateController@refresh')->name('api.refresh');
});
