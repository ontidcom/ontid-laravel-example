<?php

use App\Models\MediaLibrary;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        Role::firstOrCreate(['name' => Role::ROLE_EDITOR]);
        $role_admin = Role::firstOrCreate(['name' => Role::ROLE_ADMIN]);

        // MediaLibrary
        MediaLibrary::firstOrCreate([]);

        // Users one
        $userOne = User::firstOrCreate(
            ['email' => 'info@ontid.com'],
            ['password' => Hash::make('123456789')]
        );
        $userOne->roles()->sync([$role_admin->id]);

        $this->call([
            NewsTableSeeder::class,
            CategoryTableSeeder::class
        ]);
    }
}
