<?php

use App\Models\News;
use Faker\Generator;

$factory->define(News::class, function (Generator $faker) {
    return [
        'slug' => $faker->unique()->slug,
        'seo_title' => $faker->sentence,
        'seo_description' => $faker->sentence,
        'seo_keywords' => $faker->sentence,
        'seo_breadcrumbs' => $faker->sentence,
        'seo_h1' => $faker->sentence,
        'preview' => $faker->paragraph,
        'content' => $faker->text,
        'published' => $faker->boolean(true),
        'posted_at' => now(),
    ];
});
