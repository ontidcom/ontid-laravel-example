<?php

use App\Models\Category;
use Faker\Generator;

$factory->define(Category::class, function (Generator $faker) {
    return [
        'name' => $faker->unique()->name,
        'slug' => $faker->unique()->slug,
        'seo_title' => $faker->sentence,
        'seo_description' => $faker->sentence,
        'seo_keywords' => $faker->sentence,
        'seo_breadcrumbs' => $faker->sentence,
        'seo_h1' => $faker->sentence,
        'content' => $faker->text,
        'published' => $faker->boolean(true),
    ];
});
