<?php

use App\Models\User;
use Faker\Generator;
use Illuminate\Support\Str;

$factory->define(User::class, function (Generator $faker) {
    static $password;
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = 'password',
        'api_token' => Str::random(60),
    ];
});

$factory->state(User::class, 'ontid', function (Generator $faker) {
    return [
        'first_name' => 'Ontid',
        'last_name' => 'Ontid',
        'email' => 'into@ontid.com'
    ];
});
