<?php

return [
    /*
    |--------------------------------------------------------------------------
    | News Language Lines
    |--------------------------------------------------------------------------    |
    */

    'count' => 'News - :count|News - :count',
    'name' => 'News',
    'create' => 'Create news',
    'registrations' => 'New news',
    'edit' => 'Edit news',
    'created' => 'News created',
    'updated' => 'News updated',
    'deleted' => 'News deleted',

    'category' => [
        'count' => 'Category - :count|Categories - :count',
        'name' => 'Category',
        'create' => 'Create category',
        'edit' => 'Edit category',
        'created' => 'Category created',
        'updated' => 'Category updated',
        'deleted' => 'Category deleted',
    ],
];