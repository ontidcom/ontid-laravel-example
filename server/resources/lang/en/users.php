<?php

return [
    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------    |
    */

    'count' => 'User - :count|Users - :count',
    'registrations' => 'User Registrations',
    'attributes' => [
        'id' => 'ID',
        'first_name' => 'First name',
        'last_name' => 'Last name',
        'email' => 'Email',
        'created_at' => 'Created at',
        'updated_at' => 'Updated at'
    ]
];