<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Pages Language Lines
    |--------------------------------------------------------------------------    |
    */

    'handbooks' => [
        'count' => 'Handbook - :count|Handbooks - :count',
        'name' => 'Handbook',
        'create' => 'Create handbook',
        'edit' => 'Edit handbook',
        'created' => 'Handbook created',
        'updated' => 'Handbook updated',
        'deleted' => 'Handbook deleted',
    ],

    'minutes' => [
        'count' => 'Minute - :count|Minutes - :count',
        'name' => 'Minute',
        'create' => 'Create minute',
        'edit' => 'Edit minute',
        'created' => 'Minute created',
        'updated' => 'Minute updated',
        'deleted' => 'Minute deleted',
    ],
];