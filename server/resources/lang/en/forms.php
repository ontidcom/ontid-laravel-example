<?php

return [

    'actions' => [
        'save' => 'Save',
        'preview' => 'Preview',
        'publish' => 'Publish',
        'update' => 'Update',
        'back' => 'Back',
        'generate' => 'Generate',
        'delete' => 'Delete',
        'add' => 'Add',
    ],


    'news' => [
        'delete' => 'Are you sure you want to delete this article?',
        'delete_thumbnail' => "Are you sure you want to delete the image?"
    ],

    'tokens' => [
        'generate' => 'Are you sure you want to generate a key? All of these APIs will be disconnected.',
    ],

    'media' => [
        'delete' => 'Are you sure you want to delete this thumbnail?',
    ],

    'preview' => [
        'add' => 'Add image',
        'delete' => 'Delete',
        'format' => 'Format - JPG/JPEG/PNG.',
        'upload_image' => 'Upload images',
        'seo_title' => 'SEO Title',
        'seo_alt' => 'SEO Alt',
        'image' => 'Upload images'
    ],

    'status' => [
        'active' => 'Active',
        'disabled' => 'Disabled',
        'yes' => 'Yes',
        'no' => 'No'
    ],

    'attributes' => [
        'url' => 'URL',
        'image' => 'Image',
        'chapter' => 'Chapter',
        'section' => 'Section',
        'text' => 'Text',
        'block' => 'Block',
        'preview' => 'Preview',
        'seo' => 'SEO',
        'id' => 'ID',
        'name' => 'Name',
        'active' => 'Post status',
        'date' => 'Publication date',
        'time' => 'Publication t ime',
        'seo_title' => 'Title',
        'seo_alt' => 'seo_alt',
        'seo_description' => 'Description',
        'seo_keywords' => 'Keywords',
        'seo_breadcrumbs' => 'Breadcrumbs',
        'seo_h1' => 'H1',
        'posted_at' => 'Posted at',
        'author' => 'Author',
        'category' => 'Category',
        'content' => 'Content',
        'created_at' => 'Created at',
        'updated_at' => 'Updated at',
        'important' => 'Important',
        'date_start' => 'Date start',
        'date_end' => 'Date end',
        'location' => 'Location'
    ],

    'upload_image' => 'Upload images',
    'add_new_block' => 'Add new block',
    'delete_block' => 'Delete block'

];
