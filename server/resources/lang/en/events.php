<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Events Language Lines
    |--------------------------------------------------------------------------    |
    */

    'name' => 'event',
    'count' => 'Event - :count|Events - :count',
    'events' => 'events',
    'create' => 'Create events',
    'registrations' => 'New events',
    'edit' => 'Edit events',
    'created' => 'Event created',
    'updated' => 'Event updated',
    'deleted' => 'Event deleted',

    'location' => [
        'count' => 'Location - :count|Locations - :count',
        'name' => 'Location',
        'create' => 'Create location',
        'edit' => 'Edit location',
        'created' => 'Location created',
        'updated' => 'Location updated',
        'deleted' => 'Location deleted',
    ]
];