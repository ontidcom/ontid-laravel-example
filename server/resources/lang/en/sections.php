<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Sections Language Lines
    |--------------------------------------------------------------------------    |
    */

    'commissions' => [
        'count' => 'Commission - :count|Commissions - :count',
        'name' => 'Commission',
        'create' => 'Create commission',
        'edit' => 'Edit commission',
        'created' => 'Commission created',
        'updated' => 'Commission updated',
        'deleted' => 'Commission deleted',
    ],

    'socials' => [
        'count' => 'Social - :count|Socials - :count',
        'name' => 'Social',
        'create' => 'Create social',
        'edit' => 'Edit social',
        'created' => 'Social created',
        'updated' => 'Social updated',
        'deleted' => 'Social deleted',
    ],

    'partners' => [
        'count' => 'Partner - :count|Partners - :count',
        'name' => 'Partner',
        'create' => 'Create partner',
        'edit' => 'Edit partner',
        'created' => 'Partner created',
        'updated' => 'Partner updated',
        'deleted' => 'Partner deleted',
    ],
];