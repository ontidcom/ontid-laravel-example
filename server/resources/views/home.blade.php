@extends('layouts.app')

@section('content')
    <ul>
        <li><a href="{{ route('news')}}">@lang('news.name')</a></li>
        <li><a href="{{ route('event')}}">@lang('events.name')</a></li>
    </ul>
@endsection