<?php

namespace Tests\Unit;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test user has role
     */
    public function testUserHasRole()
    {
        $user = factory(User::class)->create();
        $role = factory(Role::class)->create();
        $user->roles()->attach($role);
        $this->assertTrue($user->hasRole($role->name));
    }

    /**
     * Test user is admin
     */
    public function testUserIsAdmin()
    {
        $admin = factory(User::class)->create();
        $role_editor = factory(Role::class)->states('editor')->create();
        $role_admin = factory(Role::class)->states('admin')->create();
        $admin->roles()->sync([$role_admin->id, $role_editor->id]);
        $this->assertTrue($admin->isAdmin());
    }

    /**
     * Test user it not admin
     */
    public function testUserIsNotAdmin()
    {
        $user = factory(User::class)->create();
        $user->roles()->attach(
            factory(Role::class)->states('editor')->create()
        );
        $this->assertFalse($user->isAdmin());
    }

    /**
     * Test first name attribute
     */
    public function testNameAttribute()
    {
        $user = factory(User::class)->create(['first_name' => 'TEST first name', 'last_name' => 'TEST last name']);
        $this->assertEquals('TEST first name', $user->first_name);
        $this->assertEquals('TEST last name', $user->last_name);
    }

    /**
     * Test full name attribute
     */
    public function testFullNameAttribute()
    {
        $user = factory(User::class)->create(['first_name' => 'TEST', 'last_name' => 'TEST']);
        $this->assertEquals('TEST TEST', $user->getFullNameAttribute());
    }

    /**
     * Test created at
     */
    public function testCreatedAt()
    {
        $user = factory(User::class)->create();
        $this->assertEquals($user->created_at->toDateTimeString(), now()->toDateTimeString());
    }
}