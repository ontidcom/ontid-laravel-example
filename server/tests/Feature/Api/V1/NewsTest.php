<?php

namespace Tests\Feature\Api\V1;

use App\Models\News;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NewsTest extends TestCase
{
    use RefreshDatabase;

    public function testPostIndex()
    {
        factory(News::class, 2)->create();

        $this->json('GET', '/api/v1/news')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [[
                    'id',
                    'slug',
                    'seo' => ['title', 'description', 'keywords', 'breadcrumbs'],
                    'h1',
                    'preview',
                    'content',
                    'category' => ['id'],
                    'published',
                    'thumbnail',
                    'posted_at',
                    'created_at',
                    'updates_at',
                ]],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ]
            ]);
    }
}
