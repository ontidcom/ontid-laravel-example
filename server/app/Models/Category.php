<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'seo_breadcrumbs',
        'seo_h1',
        'content',
        'slug',
        'published'
    ];

    /**
     * Published
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsPublished($query)
    {
        return $query->where('published', true);
    }

    /**
     * Return the category's news
     */
    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }
}
