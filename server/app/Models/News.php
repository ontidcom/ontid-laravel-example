<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class News extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'preview',
        'category_id',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'seo_breadcrumbs',
        'seo_h1',
        'content',
        'slug',
        'posted_at',
        'thumbnail_id',
        'published',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'posted_at'
    ];

    /**
     * Return the news category
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Published
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsPublished($query)
    {
        return $query->where('published', true);
    }

    /**
     * Posted at
     *
     * @param $query
     * @return mixed
     */
    public function scopePostedAt($query)
    {
        return $query->whereDate('posted_at', '<=', Carbon::today()->toDateString());
    }

    /**
     * Return the news thumbnail
     */
    public function thumbnail(): BelongsTo
    {
        return $this->belongsTo(Media::class);
    }

    /**
     * return true if the news has a thumbnail
     */
    public function hasThumbnail(): bool
    {
        return filled($this->thumbnail_id);
    }
}
