<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MediaLibraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'seo_title' => empty($this->input('seo_title')) ? '' : strip_tags($this->input('seo_title')),
            'seo_alt' => empty($this->input('seo_alt')) ? '' : strip_tags($this->input('seo_alt')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'image' => 'required',
            'name' => 'nullable|string|max:255'
        ];
    }
}
