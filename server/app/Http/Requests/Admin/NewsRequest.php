<?php

namespace App\Http\Requests\Admin;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Mews\Purifier\Facades\Purifier;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'published' => (bool)$this->input('published'),
            'posted_at' => Carbon::parse($this->input('date')),
            'slug' => Str::slug(strip_tags($this->input('seo_title'))),
            'seo_title' => strip_tags($this->input('seo_title')),
            'seo_description' => strip_tags($this->input('seo_description')),
            'seo_keywords' => strip_tags($this->input('seo_keywords')),
            'seo_breadcrumbs' => strip_tags($this->input('seo_breadcrumbs')),
            'seo_h1' => strip_tags($this->input('seo_h1')),
            'preview' => strip_tags($this->input('preview')),
            'content' => Purifier::clean($this->input('content')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'seo_title' => [
                'required',
                Rule::unique('news')->ignore(optional($this->news)->id ?: 'NULL'),
            ],
            'posted_at' => 'required|date',
            'thumbnail_id' => 'nullable|exists:media,id',
            'category_id' => 'nullable|exists:categories,id',
            'slug' => Rule::unique('news')->ignore(optional($this->news)->id ?: 'NULL'),
        ];
    }
}
