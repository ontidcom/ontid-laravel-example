<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Mews\Purifier\Facades\Purifier;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'published' => (bool)$this->input('published'),
            'slug' => Str::slug(strip_tags($this->input('name'))),
            'name' => strip_tags($this->input('name')),
            'seo_title' => strip_tags($this->input('seo_title')),
            'seo_description' => strip_tags($this->input('seo_description')),
            'seo_keywords' => strip_tags($this->input('seo_keywords')),
            'seo_breadcrumbs' => strip_tags($this->input('seo_breadcrumbs')),
            'seo_h1' => strip_tags($this->input('seo_h1')),
            'content' => Purifier::clean($this->input('content')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                Rule::unique('categories')->ignore(optional($this->category)->id ?: 'NULL'),
            ],
            'slug' => Rule::unique('categories')->ignore(optional($this->category)->id ?: 'NULL'),
        ];
    }
}
