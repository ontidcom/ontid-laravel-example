<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MediaLibraryRequest;
use App\Http\Requests\Admin\MediaRequest;
use App\Http\Resources\Media as MediaResource;
use App\Models\Media;
use App\Models\MediaLibrary;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class MediaController extends Controller
{
    /**
     * Return the media.
     *
     * @param Request $request
     * @return ResourceCollection
     */
    public function index(Request $request): ResourceCollection
    {
        return MediaResource::collection(
            MediaLibrary::first()->media()->paginate($request->input('limit', 20))
        );
    }

    /**
     * Return the one media.
     *
     * @param int $id
     * @return MediaResource
     */
    public function show(int $id): MediaResource
    {
        return new MediaResource(
            Media::findOrFail($id)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MediaLibraryRequest $request
     * @return MediaResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(MediaLibraryRequest $request): MediaResource
    {
        $this->authorize('store', Media::class);

        $image = $request->file('image');
        $filename = md5($request->file('image')->getClientOriginalName()) . '.' . $request->file('image')->getClientOriginalExtension();
        $name = $image->getClientOriginalName();

        return new MediaResource(
            MediaLibrary::first()
                ->addMedia($image)
                ->setFileName($filename)
                ->withCustomProperties(['seo' => ['title' => $seoTitle = $request->input('seo_title'), 'alt' => $request->input('seo_alt')]])
                ->usingName($name)
                ->preservingOriginal()
                ->toMediaCollection('images')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @param MediaRequest $request
     * @return MediaResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(int $id, MediaRequest $request): MediaResource
    {
        $this->authorize('store', Media::class);

        $media = Media::findOrFail($id);
        $media->setCustomProperty('seo', ['title' => $request->input('seo_title'), 'alt' => $request->input('seo_alt')]);
        $media->name = $request->input('name');
        $media->save();

        return new MediaResource($media);
    }

    /**
     * Remove the specified resource from storage
     *
     * @param Media $media
     * @return Response
     * @throws \Exception
     */
    public function destroy(Media $media): Response
    {
        $this->authorize('delete', $media);

        $media->delete();
        return response()->noContent();
    }
}
