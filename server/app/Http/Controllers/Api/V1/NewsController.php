<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewsRequest;
use App\Http\Resources\News as NewsResource;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class NewsController extends Controller
{
    /**
     * Return the posts.
     *
     * @param Request $request
     * @return ResourceCollection
     */
    public function index(Request $request): ResourceCollection
    {
        return NewsResource::collection(
            News::isPublished()->postedAt()->latest()->paginate($request->input('limit', 20))
        );
    }

    /**
     * Return the specified resource.
     *
     * @param News $news
     * @return NewsResource
     */
    public function show(News $news): NewsResource
    {
        return new NewsResource($news);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsRequest $request
     * @param News $news
     * @return NewsResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(NewsRequest $request, News $news): NewsResource
    {
        $this->authorize('update', $news);

        $news->update($request->only(['preview', 'seo_title', 'seo_description', 'seo_keywords', 'seo_breadcrumbs', 'seo_h1', 'content', 'slug', 'posted_at', 'published', 'important', 'category_id', 'thumbnail_id']));
        return new NewsResource($news);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest $request
     * @return NewsResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(NewsRequest $request): NewsResource
    {
        $this->authorize('store', News::class);

        return new NewsResource(
            News::create($request->only(['preview', 'seo_title', 'seo_description', 'seo_keywords', 'seo_breadcrumbs', 'seo_h1', 'content', 'slug', 'posted_at', 'published', 'important', 'category_id', 'thumbnail_id']))
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(News $news): Response
    {
        $this->authorize('delete', $news);
        
        $news->delete();
        return response()->noContent();
    }
}
