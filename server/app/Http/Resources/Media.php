<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Media extends Resource
{
    /**
     * Transform the resource into an array.
     * @param $request
     * @return array
     */
    public function toArray($request): array
    {
        $seoTitle = '';
        $seoAlt = '';

        if ($this->hasCustomProperty('seo.title')) {
            $seoTitle = $this->getCustomProperty('seo.title');
        }
        if ($this->hasCustomProperty('seo.alt')) {
            $seoAlt = $this->getCustomProperty('seo.alt');
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'seo' => [
                'title' => $seoTitle,
                'alt' => $seoAlt
            ],
            'url' => [
                'original' => $this->getUrl(),
                'thumb' => $this->getUrl('thumb'),
            ]
        ];
    }
}
