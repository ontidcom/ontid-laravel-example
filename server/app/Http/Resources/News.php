<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class News extends Resource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'preview' => $this->preview,
            'slug' => $this->slug,
            'seo' => [
                'title' => $this->seo_title,
                'description' => $this->seo_description,
                'keywords' => $this->seo_keywords,
                'breadcrumbs' => $this->seo_breadcrumbs,
            ],
            'h1' => $this->seo_h1,
            'content' => $this->content,
            'category' => [
                'id' => $this->category_id,
            ],
            'published' => $this->published,
            'thumbnail' => [
                'id' => $this->thumbnail_id,
                'thumb_url' => $this->when($this->hasThumbnail(), optional($this->thumbnail)->getUrl('thumb')),
                'original_url' => $this->when($this->hasThumbnail(), optional($this->thumbnail)->getUrl()),
            ],
            'posted_at' => $this->posted_at->timestamp,
            'created_at' => $this->created_at->timestamp,
            'updated_at' => $this->updated_at->timestamp,
        ];
    }
}
