<?php

namespace App\Policies;

use App\Models\News;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user is admin for all authorization.
     * @param User $user
     * @return bool
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the news.
     * @param User $user
     * @param News $news
     * @return bool
     */
    public function update(User $user, News $news): bool
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can store a news.
     * @param User $user
     * @return bool
     */
    public function store(User $user): bool
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the news.
     * @param User $user
     * @param News $news
     * @return bool
     */
    public function delete(User $user, News $news): bool
    {
        return $user->isAdmin();
    }
}
