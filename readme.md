## Descriptio

ontid is a web application.

## Installation

Development environment requirements:
- [Docker](https://www.docker.com) >= 17.06
- [Docker Compose](https://docs.docker.com/compose/install/)


## Before starting - dev stage
```bash
$ cd ontid-laravel-example/server
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
$ cp .env.dev .env
$ docker-compose run --rm --no-deps ontid-server composer install
$ docker-compose run --rm --no-deps ontid-server php artisan key:generate
$ docker-compose run --rm --no-deps ontid-server php artisan jwt:secret
$ docker-compose run --rm --no-deps ontid-server php artisan config:cache
$ docker-compose run --rm --no-deps ontid-server php artisan route:cache
$ docker-compose run --rm ontid-server php artisan migrate --seed
$ docker-compose up -d
```

## Before starting - prod stage
```bash
$ cd ontid-laravel-example/server
$ chmod -R 777 storage
$ chmod -R 777 bootstrap/cache
$ $ cp .env.prod .env
$ docker-compose run --rm --no-deps ontid-server composer install --optimize-autoloader --no-dev
$ docker-compose run --rm --no-deps ontid-server php artisan key:generate
$ docker-compose run --rm --no-deps ontid-server php artisan jwt:secret
$ docker-compose run --rm --no-deps ontid-server php artisan config:cache
$ docker-compose run --rm --no-deps ontid-server php artisan route:cache
$ docker-compose up -d
```
Now you can access the application via [http://localhost:81](http://localhost:81).

## Useful commands
You need to run the migrations with the seeds:
```bash
$ docker-compose run --rm ontid-server php artisan migrate --seed
```

Running tests:
```bash
$ ./vendor/bin/phpunit --cache-result --order-by=defects --stop-on-defect
```

Generating backup:
```bash
$ docker-compose run --rm ontid-server php artisan vendor:publish --provider="Spatie\Backup\BackupServiceProvider"
$ docker-compose run --rm ontid-server php artisan backup:run
```

In development environnement, rebuild the database:
```bash
$ docker-compose run --rm ontid-server php artisan migrate:fresh --seed
```

## Accessing the API

Clients can access to the REST API. API requests require authentication via token. You can create a new token in your user profile.

Then, you can use this token either as url parameter or in Authorization header :

```bash API
    http://localhost:81/api/v1/categories - Category
    http://localhost:81/api/v1/news - News
```

API are prefixed by ```api``` and the API version number like so ```v1```.

Do not forget to set the ```X-Requested-With``` header to ```XMLHttpRequest```. Otherwise, Laravel won't recognize the call as an AJAX request.

To list all the available routes for API :

```bash
$ docker-compose run --rm --no-deps ontid-server php artisan route:list --path=api
```
